var express = require('express');
var router = express.Router();

var deviceTokenTable = "device_token";

router.post('/updateDeviceToken', function(req, res) {
	var deviceTokenFromClient = req.body.deviceToken;
	console.log("registration id = " + deviceTokenFromClient);
	
	var sqlQuery = 'insert into `' + deviceTokenTable + '`(`token`) values ("' + deviceTokenFromClient + '")';
	console.log("sqlQuery = " + sqlQuery);
	var query = req.mysqlConn.query(sqlQuery, function(err, result) {
	  	if(err) {
	  		console.log("Error in sql insert: ", err);
	  		res.json({"success": false, "error": err});
	  	} else {
	  		res.json({"success": true});
	  	}
	});
});

router.get('/deviceTokens', function(req, res) {
	var sqlQuery = 'select * from `' + deviceTokenTable + '` order by id desc';
	console.log("sqlQuery = " + sqlQuery);
	var query = req.mysqlConn.query(sqlQuery, function(err, results, fields) {
	  	if(err) {
	  		console.log("Error in sql select: ", err);
	  		res.json({"success": false, "error": err});
	  	} else {
	  		res.json({"success": true, "deviceTokens": results});
	  	}
	});
});

module.exports = router;